// Example program

#include <iostream>
#include <random>

using namespace std;

int main()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    
    float mean = 50; // set population mean
    float std_dev = 6;   //set standard deviation
    
    int n = 20; //set sample size
    float samp_mean;
    float samp_sum = 0;
    float rand_num;
    

    
    std::normal_distribution<float> norm(mean,std_dev); //setup distribution object
 
    for(int j = 0; j < 10; j++){
    	for(int i = 0 ; i < n ; i++){// create a sample of size n
		rand_num = norm(gen);
       		cout<< rand_num<<endl;
		samp_sum += rand_num;

	}
	samp_mean = samp_sum/n;
	cout << "The sample's mean is: " <<samp_mean<<endl;
	float pos_conf_level = samp_mean + (1.96*6)/sqrt(20);
	float neg_conf_level = samp_mean - (1.96*6)/sqrt(20);
	cout<< "The sample's confidence interval is between: "<<neg_conf_level<<" and "<<pos_conf_level<<endl;
	samp_sum =0;
	samp_mean = 0; 
	pos_conf_level =0;
	neg_conf_level = 0;
    }  // print out a random number to the screen 
   
    
    //To Do: Create correct number of samples
    //       Calculate means for each sample

    //       (Calculate confidence intervals also??)
        
        
}
