#https://www.youtube.com/watch?v=aw9wHbFTnAQ 

# makefile version 1.0.02.18.2019
	
all:
	g++ -std=c++14 -g -Wall random.cpp -o rand.out

# ...................................................................

run:
	@./rand.out

# ...................................................................

# remove temp files

clean:
	rm -f *.out *.o 
